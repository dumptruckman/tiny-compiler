package xyz.jeremydwood.school.compiler.token

/**
 * Represents a token in our language.
 * @author Jeremy Wood
 */
interface Token {

    /**
     * The value of the token. For scanned tokens, this will be the string scanned in.
     */
    val value: String?

    /**
     * Whether the token is a terminal in the language's grammar.
     */
    val isTerminal: Boolean


    /**
     * Whether the token is a non-terminal in the language's grammar.
     */
    val isNonTerminal: Boolean
        get() = !isTerminal

    /**
     * Whether this kind of token should show up when producing a list of scanned tokens.
     */
    val isListable: Boolean

    /**
     * Whether the token is non-alphanumeric
     */
    val isNonWord: Boolean

    /**
     * Determines if this token matches the given other token
     *
     * @param otherToken The other token to check.
     * @return True if this token matches the given otherToken.
     */
    fun matches(otherToken: Token): Boolean

    val baseToken: EnumeratedToken
}