package xyz.jeremydwood.school.compiler.token

/**
 * All of the possible types of tokens for our language.
 * @author Jeremy Wood
 */
enum class EnumeratedToken(override val value: String? = null,
                           override val isNonWord: Boolean = false,
                           override val isListable: Boolean = true) : Token {
    IDENTIFIER,
    NUMBER,
    OPEN_PARENTHESIS("(", true),
    CLOSED_PARENTHESIS(")", true),
    READ("read"),
    WRITE("write"),
    BEGIN("begin"),
    END("end"),
    IF("if"),
    THEN("then"),
    ELSE("else"),
    END_IF("endif"),
    WHILE("while"),
    END_WHILE("endwh"),
    PLUS("+", true),
    MINUS("-", true),
    TIMES("*", true),
    DIVIDE("/", true),
    ASSIGNMENT(":=", true),
    STATEMENT_TERMINATOR(";", true),

    ERROR,
    WHITESPACE(isListable = false),
    LINE_SEPARATOR(isListable = false),
    EOF(isListable = false),
    BOF(isListable = false),
    ;

    override val isTerminal: Boolean
        get() = value == null

    override fun matches(otherToken: Token): Boolean = when (otherToken) {
        is EnumeratedToken -> otherToken == this
        is ScannedToken -> otherToken.baseToken == this
        else -> false
    }

    override val baseToken: EnumeratedToken
        get() = this

    companion object {

        private val stringTokenMap = mutableMapOf<String, EnumeratedToken>()
        private val nonWordTokens = mutableSetOf<Char>()

        init {
            for (t in EnumeratedToken.values()) {
                val value = t.value;
                if (value != null) {
                    stringTokenMap[value] = t

                    if (t.isNonWord && value.isNotEmpty()) {
                        nonWordTokens.add(value[0])
                    }
                }
            }
        }

        /**
         * Returns the Token that matches the given character sequence or null if none match.
         *
         * Treat this as a static method of the EnumeratedToken class.
         */
        @JvmStatic
        fun fromString(stringForm: CharSequence): EnumeratedToken? {
            return stringTokenMap[stringForm.toString()]
        }

        /**
         * Returns true if the given character starts a non-word token.
         *
         * Treat this as a static method of the EnumeratedToken class.
         */
        @JvmStatic
        fun isStartOfNonWordToken(char: Char): Boolean {
            return nonWordTokens.contains(char);
        }
    }
}
