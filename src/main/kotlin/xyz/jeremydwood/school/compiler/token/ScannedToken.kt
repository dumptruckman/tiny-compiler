package xyz.jeremydwood.school.compiler.token

/**
 * Represents a token that was scanned in and contains the scanned in value.
 * @author Jeremy Wood
 */
class ScannedToken internal constructor(override val baseToken: EnumeratedToken,
                                        private val scannedValue: String,
                                        private val line: Integer,
                                        private val column: Integer) : Token by baseToken {
    override val value: String
        get() = scannedValue

    override fun toString(): String {
        return "$baseToken( value=$scannedValue line=$line column=$column )"
    }

    override fun matches(otherToken: Token): Boolean = when (otherToken) {
        is EnumeratedToken -> otherToken == this.baseToken
        is ScannedToken -> otherToken.baseToken == this.baseToken
        else -> false
    }
}