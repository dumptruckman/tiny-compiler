package xyz.jeremydwood.school.compiler

import com.xenomachina.argparser.mainBody
import xyz.jeremydwood.school.compiler.grammar.Grammar
import xyz.jeremydwood.school.compiler.grammar.GrammarError
import xyz.jeremydwood.school.compiler.grammar.GrammarResult
import xyz.jeremydwood.school.compiler.grammar.ProductionResult
import java.io.FileNotFoundException
import java.io.FileReader
import java.io.IOException
import java.io.Reader

/**
 * The main program class.
 * @author Jeremy Wood
 */
class Main {

    companion object {

        @JvmStatic
        fun main(args: Array<String>) = mainBody {
            val opts = ProgramOptions(args)

            try {
                if (opts.scanOnly) {
                    FileReader(opts.source.toFile()).use { reader ->
                        TinyScanner.printTokens(reader)
                    }
                } else {
                    FileReader(opts.source.toFile()).use { reader ->
                        val result = TinyParser(reader).parse()
                        val errors = result.getErrorsRecursively()
                        if (errors.isEmpty()) {
                            println("Parsed successfully!")
                        } else {
                            errors.forEach { e -> println(e.message) }
                        }
                    }
                }
            } catch (e: FileNotFoundException) {
                System.err.println("Could find source file")
                System.exit(1)
            } catch (e: IOException) {
                System.err.print("There was an error reading or loading the source file: " + e.message)
                System.exit(1)
            }

        }
    }
}