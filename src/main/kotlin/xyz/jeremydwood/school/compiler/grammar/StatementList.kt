package xyz.jeremydwood.school.compiler.grammar

import xyz.jeremydwood.school.compiler.LookaheadScanner

class StatementList(grammar: Grammar) : ProductionRule(grammar) {

    override val typeName: String = "Statement List"
    override val containsTerminals: Boolean = false

    override fun parse(scanner: LookaheadScanner): ProductionResult = compileResults(scanner) {
        parse(Statement::class, "statement", false)
        parse(StatementList(grammar))
    }
}