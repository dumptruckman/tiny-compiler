package xyz.jeremydwood.school.compiler.grammar

import xyz.jeremydwood.school.compiler.LookaheadScanner

/**
 * The expression production rule.
 * @author Jeremy Wood
 */
class Expression(grammar: Grammar) : ProductionRule(grammar) {

    override val typeName: String = "Expression"

    override val containsTerminals: Boolean = false

    override fun parse(scanner: LookaheadScanner): ProductionResult = compileResults(scanner) {
        parse(Term(grammar))
        parse(TermTail(grammar))
    }
}