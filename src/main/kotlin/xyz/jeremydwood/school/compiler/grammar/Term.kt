package xyz.jeremydwood.school.compiler.grammar

import xyz.jeremydwood.school.compiler.LookaheadScanner

/**
 * The term production rule.
 * @author Jeremy Wood
 */
class Term(grammar: Grammar) : ProductionRule(grammar) {

    override val typeName: String = "Term"

    override val containsTerminals: Boolean = false

    override fun parse(scanner: LookaheadScanner): ProductionResult = compileResults(scanner) {
        parse(Factor(grammar), "factor")
        parse(FactorTail(grammar), stopOnEpsilon = false)
    }
}