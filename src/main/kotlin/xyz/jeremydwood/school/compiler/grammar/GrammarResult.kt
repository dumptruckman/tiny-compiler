package xyz.jeremydwood.school.compiler.grammar

import xyz.jeremydwood.school.compiler.TreeNode
import xyz.jeremydwood.school.compiler.token.Token

/**
 * The result of a grammar action.
 * @author Jeremy Wood
 *
 * @constructor
 * @param error Any error associated with the grammar action.
 * @param recursiveResults Results that occurred as recursive actions of this result's grammar action.
 */
open class GrammarResult(error: GrammarError? = null, vararg recursiveResults: GrammarResult) {

    /**
     * The errors associated with this result's grammar action.
     */
    val errors: List<GrammarError> = mutableListOf()

    @Suppress("LeakingThis")
    private val resultTree: TreeNode<GrammarResult> = TreeNode(this)

    internal fun addResult(result: GrammarResult) {
        resultTree.addChild(result.resultTree)
    }

    internal fun addError(result: GrammarError) {
        (errors as MutableList<GrammarError>).add(result)
    }

    init {
        if (error != null) {
            addError(error)
        }
        recursiveResults.forEach { resultTree.addChild(it.resultTree) }
    }

    /**
     * Returns all of the errors associated with this grammar result and its recursive results.
     */
    fun getErrorsRecursively(): List<GrammarError> {
        val allErrors: MutableList<GrammarError> = mutableListOf()
        errors.forEach {
            allErrors.add(it)
        }
        resultTree.forEach {
            allErrors.addAll(it.data.getErrorsRecursively());
        }
        return allErrors;
    }
}

/**
 * The result of a match action in the grammar, where a token is matched in a production rule.
 * @author Jeremy Wood
 *
 * @constructor
 * @param matchedToken The token that was ultimately matched.
 * @param error Any error that occur while matching.
 * @param recursiveResults Results that occurred as recursive actions of this result's grammar action.
 */
class MatchResult(val matchedToken: Token,
                  error: GrammarError? = null,
                  vararg recursiveResults: GrammarResult)
    : GrammarResult(error, *recursiveResults) {

    /**
     * Creates a MatchResult when an attempted match causes further production rules to be matched.
     *
     * @param error Any error that occurs while matching.
     * @param recursiveProductionResult The result of the recursive production rule.
     * @param recursiveMatchResult The result of the recursive match.
     */
    constructor(error: GrammarError? = null, recursiveProductionResult: ProductionResult, recursiveMatchResult: MatchResult)
            : this(recursiveMatchResult.matchedToken, error, recursiveProductionResult, recursiveMatchResult)
}

/**
 * The overall result of a production rule.
 * @author Jeremy Wood
 *
 * @constructor
 * @param matchedProduction The production rule that was ultimately matched.
 * @param error Any error that occur while matching.
 * @param recursiveResults Results that occurred as recursive actions of this result's grammar action.
 */
class ProductionResult(val matchedProduction: ProductionRule?,
                       error: GrammarError? = null,
                       vararg recursiveResults: GrammarResult)
    : GrammarResult(error, *recursiveResults)