package xyz.jeremydwood.school.compiler.grammar

import xyz.jeremydwood.school.compiler.LookaheadScanner

import xyz.jeremydwood.school.compiler.token.EnumeratedToken.*

/**
 * The program production rule.
 * @author Jeremy Wood
 */
class Program(grammar: Grammar) : ProductionRule(grammar) {

    override val typeName: String = "Program"

    override fun parse(scanner: LookaheadScanner): ProductionResult = compileResults(scanner) {
        match(BEGIN)
        parse(StatementList(grammar))
        match(END)
        match(EOF)
    }
}