package xyz.jeremydwood.school.compiler.grammar

import xyz.jeremydwood.school.compiler.LookaheadScanner

import xyz.jeremydwood.school.compiler.token.EnumeratedToken.*

/**
 * Operator production rule parent class.
 * @author Jeremy Wood
 */
sealed class Operator(grammar: Grammar) : ProductionRule(grammar) {
    override val typeName: String = "Operator"
}

/**
 * The add operator production rule.
 * @author Jeremy Wood
 */
class AddOperator(grammar: Grammar) : Operator(grammar) {
    override fun parse(scanner: LookaheadScanner): ProductionResult = compileResults(scanner) {
        if (currentToken.matches(PLUS)) {
            match(PLUS)
        } else {
            match(MINUS)
        }
    }
}

/**
 * The multiply operator production rule.
 * @author Jeremy Wood
 */
class MultiplyOperator(grammar: Grammar) : Operator(grammar) {
    override fun parse(scanner: LookaheadScanner): ProductionResult = compileResults(scanner) {
        if (currentToken.matches(TIMES)) {
            match(TIMES)
        } else {
            match(DIVIDE)
        }
    }
}
