package xyz.jeremydwood.school.compiler.grammar

import xyz.jeremydwood.school.compiler.LookaheadScanner

/**
 * A generic production rule that really represents no production rule being found. Used to stop recursive descent.
 */
class Epsilon(grammar: Grammar) : ProductionRule(grammar) {
    override val typeName: String = "Epsilon"

    init {
        isFullyParsed = true // Stops recursive descent
    }

    override fun parse(scanner: LookaheadScanner): ProductionResult {
        scanner.nextToken()
        return ProductionResult(this)
    }
}
