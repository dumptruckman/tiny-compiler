package xyz.jeremydwood.school.compiler.grammar

import xyz.jeremydwood.school.compiler.LookaheadScanner

import xyz.jeremydwood.school.compiler.token.EnumeratedToken.*

/**
 * The statement production rule which is always one of the children of this sealed class.
 * @author Jeremy Wood
 */
sealed class Statement(grammar: Grammar) : ProductionRule(grammar) {
    override val typeName: String = "Statement"
}

/**
 * The assignment statement production rule.
 * @author Jeremy Wood
 */
class AssignmentStatement(grammar: Grammar) : Statement(grammar) {
    override fun parse(scanner: LookaheadScanner): ProductionResult = compileResults(scanner) {
        match(IDENTIFIER)
        match(ASSIGNMENT)
        parse(Expression(grammar))
        match(STATEMENT_TERMINATOR)
    }
}

/**
 * The read statement production rule.
 * @author Jeremy Wood
 */
class ReadStatement(grammar: Grammar) : Statement(grammar) {
    override fun parse(scanner: LookaheadScanner): ProductionResult = compileResults(scanner) {
        match(READ)
        match(IDENTIFIER)
        match(STATEMENT_TERMINATOR)
    }
}

/**
 * The write statement production rule.
 * @author Jeremy Wood
 */
class WriteStatement(grammar: Grammar) : Statement(grammar) {
    override fun parse(scanner: LookaheadScanner): ProductionResult = compileResults(scanner) {
        match(WRITE)
        parse(Expression(grammar))
        match(STATEMENT_TERMINATOR)
    }
}

/**
 * The if statement production rule.
 * @author Jeremy Wood
 */
class IfStatement(grammar: Grammar) : Statement(grammar) {
    override fun parse(scanner: LookaheadScanner): ProductionResult = compileResults(scanner) {
        match(IF)
        match(OPEN_PARENTHESIS)
        parse(Expression(grammar))
        match(CLOSED_PARENTHESIS)
        match(THEN)
        parse(StatementList(grammar))
        match(ELSE)
        parse(StatementList(grammar))
        match(END_IF)
    }
}

/**
 * The while statement production rule.
 * @author Jeremy Wood
 */
class WhileStatement(grammar: Grammar) : Statement(grammar) {
    override fun parse(scanner: LookaheadScanner): ProductionResult = compileResults(scanner) {
        match(WHILE)
        match(OPEN_PARENTHESIS)
        parse(Expression(grammar))
        match(CLOSED_PARENTHESIS)
        parse(StatementList(grammar))
        match(END_WHILE)
    }
}