package xyz.jeremydwood.school.compiler.grammar

import xyz.jeremydwood.school.compiler.LookaheadScanner

/**
 * The term tail production rule.
 * @author Jeremy Wood
 */
class TermTail(grammar: Grammar) : ProductionRule(grammar) {

    override val typeName: String = "Term Tail"

    override val containsTerminals: Boolean = false

    override fun parse(scanner: LookaheadScanner): ProductionResult = compileResults(scanner) {
        parse(AddOperator(grammar), "operator", errorOnEpsilon = false)
        parse(Term(grammar), stopOnEpsilon = false)
        parse(TermTail(grammar), stopOnEpsilon = false)
    }
}