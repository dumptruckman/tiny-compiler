package xyz.jeremydwood.school.compiler.grammar

import xyz.jeremydwood.school.compiler.LookaheadScanner
import xyz.jeremydwood.school.compiler.token.Token

import xyz.jeremydwood.school.compiler.token.EnumeratedToken.*

/**
 * The factor production rule.
 * @author Jeremy Wood
 */
class Factor(grammar: Grammar) : ProductionRule(grammar) {

    private var token: Token? = null

    override val typeName: String
        get() = token?.value ?: "Factor"

    override fun parse(scanner: LookaheadScanner): ProductionResult = compileResults(scanner) {
        token = currentToken
        when {
            currentToken.matches(IDENTIFIER) -> match(IDENTIFIER)
            currentToken.matches(NUMBER) -> match(NUMBER)
            else -> {
                match(OPEN_PARENTHESIS)
                parse(Expression(grammar), "expression")
                match(CLOSED_PARENTHESIS)
            }
        }
    }
}
