package xyz.jeremydwood.school.compiler.grammar

import xyz.jeremydwood.school.compiler.token.Token

/**
 * Created when there is a grammar error when parsing the grammar. Tracks the expected and actual parse results.
 * @author Jeremy Wood
 *
 * @constructor
 * Create a new GrammarError.
 * @param currentToken The current token that generated this error.
 * @param expected What was expected.
 * @param actual What was found instead.
 */
class GrammarError(val currentToken: Token, expected: String, actual: String) {

    /**
     * A convenience constructor for a GrammarError.
     * @param currentToken The current token that generated this error.
     * @param expected What was expected.
     * @param actualProductionRule The production rule that was found instead.
     */
    constructor (currentToken: Token, expected: String, actualProductionRule: ProductionRule)
            : this(currentToken, expected, actualProductionRule.typeName)

    /**
     * This error's message.
     */
    val message = "Expected $expected but found $actual when parsing $currentToken"
}