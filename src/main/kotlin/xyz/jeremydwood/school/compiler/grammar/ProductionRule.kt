package xyz.jeremydwood.school.compiler.grammar

import xyz.jeremydwood.school.compiler.LookaheadScanner
import xyz.jeremydwood.school.compiler.token.EnumeratedToken
import xyz.jeremydwood.school.compiler.token.Token
import kotlin.reflect.KClass

/**
 * A production rule in a grammar.
 * @author Jeremy Wood
 *
 * @constructor
 * @param grammar The grammar this production rule is associated with.
 */
abstract class ProductionRule(protected val grammar: Grammar) {

    /**
     * Whether or not the rule is fully parsed. Fully parsed rules will not produce any further recursive descent.
     */
    var isFullyParsed: Boolean = false
        protected set

    /**
     * The name of this production rule.
     */
    abstract val typeName: String

    /**
     * Whether or not the production rule contains any terminals.
     */
    open val containsTerminals: Boolean = true

    /**
     * Parses this production rule by reading from the given scanner.
     *
     * @param scanner The token scanner that reads the program.
     * @return The result of the parse.
     */
    abstract fun parse(scanner: LookaheadScanner): ProductionResult

    /**
     * Attempts to match a terminal in this production rule.
     *
     * @param scanner The scanner that reads the program.
     * @param token The token to match.
     * @param alternateTokens Alternate tokens to match.
     * @return The result of the match which will indicate which token was actually matched.
     */
    fun match(scanner: LookaheadScanner, token: Token, vararg alternateTokens: Token): MatchResult {
        val currentToken = scanner.currentToken

        // println("${this::class.java.simpleName} is matching $token with currentToken=${scanner.currentToken} and lookahead=${scanner.lookahead}")

        if (currentToken.matches(token)) {
            return createPositiveMatch(token, scanner)
        } else {
            for (t in alternateTokens) {
                if (currentToken.matches(t)) {
                    return createPositiveMatch(t, scanner)
                }
            }

            if (currentToken.matches(EnumeratedToken.EOF)) {
                return MatchResult(EnumeratedToken.EOF, GrammarError(currentToken, token.value ?: token.toString(), "EOF"))
            }

            // Follow possible production
            val productionResult = handleNonMatch(currentToken, scanner)
            // println("Production found: ${productionResult.matchedProduction}")

            // Attempt original match again
            val recursiveMatchResult = match(scanner, token, *alternateTokens)

            // Generate error
            val error = if (productionResult.matchedProduction != null) {
                GrammarError(currentToken, token.value ?: token.toString(), productionResult.matchedProduction)
            } else {
                GrammarError(currentToken, token.value ?: token.toString(),
                        currentToken.value ?: currentToken.toString())
            }

            // println("${this::class.java.simpleName} has produced an error: ${error.message}")

            return MatchResult(error, productionResult, recursiveMatchResult)
        }
    }

    private fun createPositiveMatch(token: Token, scanner: LookaheadScanner): MatchResult {
        scanner.nextToken()
        // println("Found positive match for $token")
        return MatchResult(token)
    }

    private fun handleNonMatch(currentToken: Token, scanner: LookaheadScanner): ProductionResult {
        return grammar.getProductionRule(currentToken, scanner).parse(scanner)
    }

    /**
     * Compiles the results of a production rule parse.
     *
     * @param scanner The scanner that reads the program.
     * @param parseBlock The block of code that does the parsing for this production rule.
     * @return The compiled result of the parse.
     */
    protected fun compileResults(scanner: LookaheadScanner, parseBlock: ResultCompiler.() -> Unit): ProductionResult {
        val resultCompiler = ResultCompiler(scanner)
        try {
            resultCompiler.apply(parseBlock)
        } catch (ignore: CompilerException) { }
        return resultCompiler.compiledResult
    }

    /**
     * Holds the state the compilation of this production rule's parse results.
     *
     * @constructor
     * @param scanner The scanner that reads the program.
     */
    protected inner class ResultCompiler(private val scanner: LookaheadScanner) {
        /**
         * The final result of the compilation.
         */
        val compiledResult = ProductionResult(this@ProductionRule)

        /**
         * The current token on the scanner.
         */
        val currentToken: Token
            get() = scanner.currentToken

        /**
         * Does the same thing as [ProductionRule.match] but uses the scanner stored in this ResultCompiler and then
         * adds the results to [compiledResult].
         */
        fun match(token: Token, vararg alternateTokens: Token): Token {
            val result = this@ProductionRule.match(scanner, token, *alternateTokens)
            compiledResult.addResult(result)
            return result.matchedToken
        }

        /**
         * Parses a production rule as part of this production rule and adds the results to the compiled results.
         *
         * @param productionRule The rule to parse.
         * @param expected A string representing what was expected to be parsed.
         * @param errorOnEpsilon Whether or not to create an error if the parse isn't what is expected.
         * @param stopOnEpsilon Allows the result compilation to terminate if the parse isn't what is expected.
         */
        fun parse(productionRule: ProductionRule, expected: String? = null, errorOnEpsilon: Boolean = true, stopOnEpsilon: Boolean = true) {
            // println("Parsing $productionRule")
            if (productionRule.containsTerminals) {
                val parsedRule = getProductionRule(currentToken)
                if (parsedRule is Epsilon) {
                    if (expected != null && errorOnEpsilon) {
                        error(GrammarError(currentToken, expected, currentToken.value ?: currentToken.toString()))
                    }
                    if (stopOnEpsilon) {
                        throw CompilerException()
                    } else {
                        parse(productionRule, expected, errorOnEpsilon, stopOnEpsilon)
                    }
                } else if (!productionRule::class.java.isInstance(parsedRule)) {
                    parse(parsedRule)
                    parse(productionRule, expected, errorOnEpsilon, stopOnEpsilon)
                } else {
                    val result = productionRule.parse(scanner)
                    compiledResult.addResult(result)
                }
            } else {
                val result = productionRule.parse(scanner)
                compiledResult.addResult(result)
            }
        }

        /**
         * Parses the production rule started by the scanner's current token until the parse doesn't go as expected.
         */
        fun parse(productionType: KClass<out ProductionRule>, expected: String, errorOnEpsilon: Boolean = true) {
            val productionRule = getProductionRule(currentToken)

            if (productionType.java.isInstance(productionRule)) {
                parse(productionRule)
                return
            } else if (productionRule is Epsilon) {
                if (errorOnEpsilon) {
                    error(GrammarError(currentToken, expected, currentToken.value ?: currentToken.toString()))
                }
                // scanner.nextToken()
                throw CompilerException()
            }

            error(GrammarError(currentToken, expected, productionRule))
            parse(productionRule)
            return parse(productionType, expected)
        }

        private fun error(error: GrammarError) {
            compiledResult.addError(error)
        }

        private fun getProductionRule(token: Token): ProductionRule = grammar.getProductionRule(token, scanner)
    }

    private class CompilerException : RuntimeException()
}
