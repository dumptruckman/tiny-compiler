package xyz.jeremydwood.school.compiler.grammar

import xyz.jeremydwood.school.compiler.LookaheadScanner

/**
 * The factor tail production rule.
 * @author Jeremy Wood
 */
class FactorTail(grammar: Grammar) : ProductionRule(grammar) {

    override val typeName: String = "Term Tail"

    override val containsTerminals: Boolean = false

    override fun parse(scanner: LookaheadScanner): ProductionResult = compileResults(scanner) {
        parse(MultiplyOperator(grammar), "operator", errorOnEpsilon = false)
        parse(Factor(grammar), stopOnEpsilon = false)
        parse(FactorTail(grammar), stopOnEpsilon = false)
    }
}