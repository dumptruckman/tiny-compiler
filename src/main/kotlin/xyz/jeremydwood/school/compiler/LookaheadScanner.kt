package xyz.jeremydwood.school.compiler

import xyz.jeremydwood.school.compiler.token.Token
import java.io.Closeable
import java.io.IOException

/**
 * A token scanner with a single token lookahead.
 * @author Jeremy Wood
 */
interface LookaheadScanner : Closeable {

    /**
     * The token at the head position of this scanner. A value of BOF indicates no token has been fully scanned yet.
     */
    val currentToken: Token

    /**
     * The next token that will be returned by [nextToken].
     */
    val lookahead: Token

    /**
     * Returns the next token from this compiler. EOF indicates there are no more tokens to scan.
     * This will set [currentToken] to the current [lookahead] and read a new token into the lookahead.
     *
     * @return The next token from this compiler.
     * @throws IOException If anything goes wrong while scanning tokens.
     */
    @Throws(IOException::class)
    fun nextToken(): Token

}