package xyz.jeremydwood.school.compiler

/**
 * A very simple tree data structure.
 * @author Jeremy Wood
 *
 * @constructor
 * Create a new tree node for the given data with the given children.
 *
 * @param data The data for this node.
 * @param children Any child data to assign during construction.
 */
class TreeNode<T>(val data: T, vararg children: T) : Iterable<TreeNode<T>> {

    private val children: MutableList<TreeNode<T>> = mutableListOf()

    init {
        children.forEach { this.children.add(TreeNode(it)) }
    }

    override fun iterator(): Iterator<TreeNode<T>> = children.iterator()

    /**
     * Adds a child node to this node.
     */
    fun addChild(child: TreeNode<T>) {
        children.add(child)
    }
}