package xyz.jeremydwood.school.compiler

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.mainBody
import java.nio.file.Path
import java.nio.file.Paths

/**
 * The program options available when running TinyScanner from the command line.
 * @author Jeremy Wood
 *
 * Not many options yet...
 */
class ProgramOptions(args: Array<String>) {

    private val parser: ArgParser = ArgParser(args)

    val source: Path by parser.positional("SOURCE", help = "name of source file") { Paths.get(this) }
    val scanOnly: Boolean by parser.flagging("-s", "--scan", help = "only perform token scanning")

    companion object {

        @JvmStatic
        fun wrapMain(body: Runnable) {
            mainBody(body = { body.run() })
        }
    }
}
