package xyz.jeremydwood.school.compiler;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import xyz.jeremydwood.school.compiler.token.EnumeratedToken;
import xyz.jeremydwood.school.compiler.token.ScannedToken;
import xyz.jeremydwood.school.compiler.token.Token;

import java.io.IOException;
import java.io.PushbackReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

import static java.lang.Character.isDigit;
import static java.lang.Character.isLetter;
import static java.lang.Character.isWhitespace;
import static xyz.jeremydwood.school.compiler.token.EnumeratedToken.isStartOfNonWordToken;

/**
 * The main class that scans the contents of a file and lists the tokens it contains.
 * @author Jeremy Wood
 */
public class TinyScanner implements LookaheadScanner {

    @NotNull
    static List<Token> getAllTokens(@NotNull Reader reader) throws IOException {
        try (TinyScanner scanner = new TinyScanner(reader)) {
            return scanner.getAllTokens();
        }
    }

    static void printTokens(@NotNull Reader reader) throws IOException {
        for (Token t : getAllTokens(reader)) {
            System.out.println(t);
        }
    }

    @NotNull
    private final PushbackReaderWrapper reader;
    @NotNull
    private final StringBuilder buffer = new StringBuilder();

    private Token currentToken = EnumeratedToken.BOF;
    private Token lookahead;

    private int currentLine = 1;
    private int currentColumn = 0;
    private int columnBeforeToken = 0;
    private LineSeparator lineSeparator = LineSeparator.UNDETERMINED;

    TinyScanner(@NotNull Reader inputReader) throws IOException {
        this.reader = new PushbackReaderWrapper(inputReader);
        this.lookahead = _nextToken();
    }

    @NotNull
    private List<Token> getAllTokens() throws IOException {
        List<Token> tokens = new ArrayList<>();

        while (true) {
            Token token = nextToken();
            if (token == EnumeratedToken.EOF) {
                break;
            }
            tokens.add(token);
        }

        return tokens;
    }

    @NotNull
    @Override
    public Token getCurrentToken() {
        return currentToken;
    }

    @NotNull
    @Override
    public Token getLookahead() {
        return lookahead;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("WeakerAccess")
    @NotNull
    public Token nextToken() throws IOException {
        currentToken = lookahead;
        lookahead = _nextToken();
        return currentToken;
    }

    private Token _nextToken() throws IOException {
        try {
            Token possibleWhitespace = consumeWhitespace();
            if (possibleWhitespace == EnumeratedToken.EOF || possibleWhitespace == EnumeratedToken.LINE_SEPARATOR) {
                return possibleWhitespace;
            }
            char firstChar = (char) reader.read();
            columnBeforeToken = currentColumn;
            if (isLetter(firstChar)) {
                return scanForIdentifier(firstChar);
            } else if (isDigit(firstChar)) {
                return scanForNumber(firstChar);
            } else if (isStartOfNonWordToken(firstChar)) {
                return scanForNonWordToken(firstChar);
            } else {
                return consumeError(firstChar);
            }
        } catch (InternalIOException e) {
            // This probably isn't very proper but it seems to work well enough to unwrap the internal exception wrapper
            throw (IOException) e.getCause();
        } finally {
            resetBuffer();
        }
    }

    @NotNull
    private Token scanToEOF(@NotNull Function<Character, Token> whileBody, @NotNull Supplier<Token> eofResult) {
        int next;
        while ((next = reader.read()) != -1) {
            Token result = whileBody.apply((char) next);
            if (result != null) {
                return result;
            }
        }
        return eofResult.get();
    }

    // Return this in whileBody for scanToEOF to indicate continue reading in tokens.
    @Nullable
    private Token continueScanning(@NotNull Character nextChar) {
        buffer.append(nextChar);
        return null;
    }

    @NotNull
    private Token consumeWhitespace() {
        return scanToEOF(nextChar -> {
            if (isLineSeparator(nextChar)) {
                return null;
            } else if (isWhitespace(nextChar)) {
                return null;
            } else {
                reader.unread(nextChar);
                return EnumeratedToken.WHITESPACE;
            }
        }, () -> EnumeratedToken.EOF);
    }

    @NotNull
    private Token scanForIdentifier(char firstChar) {
        buffer.append(firstChar);

        return scanToEOF(nextChar -> {
            if (isLetter(nextChar) || isDigit(nextChar)) {
                return continueScanning(nextChar);
            } else if (isStartOfNonWordToken(nextChar) || isWhitespace(nextChar)) {
                reader.unread(nextChar);
                return buildIdentifierToken();
            } else {
                return consumeError(nextChar);
            }
        }, this::buildIdentifierToken);
    }

    @NotNull
    private Token buildIdentifierToken() {
        EnumeratedToken t = EnumeratedToken.fromString(buffer);
        return new ScannedToken(t != null ? t : EnumeratedToken.IDENTIFIER, buffer.toString(), currentLine, columnBeforeToken);
    }

    @NotNull
    private Token scanForNumber(char firstChar) {
        buffer.append(firstChar);

        return scanToEOF(nextChar -> {
            if (isDigit(nextChar)) {
                return continueScanning(nextChar);
            } else if (isStartOfNonWordToken(nextChar) || isWhitespace(nextChar)) {
                reader.unread(nextChar);
                return buildNumberToken();
            } else {
                return consumeError(nextChar);
            }
        }, this::buildNumberToken);
    }

    @NotNull
    private Token buildNumberToken() {
        return new ScannedToken(EnumeratedToken.NUMBER, buffer.toString(), currentLine, columnBeforeToken);
    }

    @NotNull
    private Token consumeError(char firstChar) {
        buffer.append(firstChar);

        return scanToEOF(nextChar -> {
            if (isWhitespace(nextChar) || isStartOfNonWordToken(nextChar)) {
                reader.unread(nextChar);
                return buildErrorToken();
            } else {
                return continueScanning(nextChar);
            }
        }, this::buildErrorToken);
    }

    @NotNull
    private Token buildErrorToken() {
        return new ScannedToken(EnumeratedToken.ERROR, buffer.toString(), currentLine, columnBeforeToken);
    }

    @NotNull
    private Token scanForNonWordToken(char firstChar) {
        buffer.append(firstChar);

        return scanToEOF(nextChar -> {
            EnumeratedToken t = EnumeratedToken.fromString(buffer);
            if (t != null) {
                reader.unread(nextChar);
                return new ScannedToken(t, buffer.toString(), currentLine, columnBeforeToken);
            }
            if (isLetter(nextChar) || isDigit(nextChar) || isWhitespace(nextChar)) {
                reader.unread(nextChar);
                return buildNonWordToken();
            } else {
                return continueScanning(nextChar);
            }
        }, this::buildNonWordToken);
    }

    @NotNull
    private Token buildNonWordToken() {
        EnumeratedToken t = EnumeratedToken.fromString(buffer);
        return new ScannedToken(t != null ? t : EnumeratedToken.ERROR, buffer.toString(), currentLine, columnBeforeToken);
    }

    private void lineSeparatorFound() {
        currentLine++;
        currentColumn = 0;
    }

    private boolean isLineSeparator(char firstChar) {
        switch (lineSeparator) {
            case UNDETERMINED:
                if (firstChar == '\r') {
                    char secondChar = (char) reader.read();
                    if (secondChar == '\n') {
                        lineSeparator = LineSeparator.WINDOWS;
                        lineSeparatorFound();
                        return true;
                    }
                    reader.unread(secondChar);
                    lineSeparator = LineSeparator.MAC;
                    lineSeparatorFound();
                    return true;
                } else if (firstChar == '\n') {
                    lineSeparator = LineSeparator.LINUX;
                    lineSeparatorFound();
                    return true;
                }
                return false;
            case MAC:
                if (firstChar == '\r') {
                    lineSeparatorFound();
                    return true;
                }
                return false;
            case LINUX:
                if (firstChar == '\n') {
                    lineSeparatorFound();
                    return true;
                }
                return false;
            case WINDOWS:
                if (firstChar == '\r') {
                    char secondChar = (char) reader.read();
                    if (secondChar == '\n') {
                        lineSeparatorFound();
                        return true;
                    }
                    reader.unread(secondChar);
                    return false;
                }
                return false;
        }
        return false;
    }

    private void resetBuffer() {
        buffer.setLength(0);
    }

    @Override
    public void close() throws IOException {
        reader.close();
    }

    private class PushbackReaderWrapper extends PushbackReader {
        private PushbackReaderWrapper(Reader in) {
            super(in);
        }

        @Override
        public int read() {
            try {
                int res = super.read();
                currentColumn++;
                return res;
            } catch (IOException e) {
                throw new InternalIOException(e);
            }
        }

        @Override
        public void unread(int c) {
            try {
                super.unread(c);
                currentColumn--;
            } catch (IOException e) {
                throw new InternalIOException(e);
            }
        }
    }

    private static class InternalIOException extends RuntimeException {
        private InternalIOException(Throwable cause) {
            super(cause);
        }
    }

    @Override
    public String toString() {
        return "TinyScanner{" +
                "currentToken=" + currentToken +
                ", lookahead=" + lookahead +
                '}';
    }

    private enum LineSeparator {
        LINUX, MAC, WINDOWS, UNDETERMINED
    }
}
