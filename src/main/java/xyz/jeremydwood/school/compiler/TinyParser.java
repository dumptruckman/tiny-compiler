package xyz.jeremydwood.school.compiler;

import org.jetbrains.annotations.NotNull;
import xyz.jeremydwood.school.compiler.grammar.AddOperator;
import xyz.jeremydwood.school.compiler.grammar.AssignmentStatement;
import xyz.jeremydwood.school.compiler.grammar.Epsilon;
import xyz.jeremydwood.school.compiler.grammar.Factor;
import xyz.jeremydwood.school.compiler.grammar.Grammar;
import xyz.jeremydwood.school.compiler.grammar.GrammarResult;
import xyz.jeremydwood.school.compiler.grammar.IfStatement;
import xyz.jeremydwood.school.compiler.grammar.MultiplyOperator;
import xyz.jeremydwood.school.compiler.grammar.ProductionRule;
import xyz.jeremydwood.school.compiler.grammar.Program;
import xyz.jeremydwood.school.compiler.grammar.ReadStatement;
import xyz.jeremydwood.school.compiler.grammar.WhileStatement;
import xyz.jeremydwood.school.compiler.grammar.WriteStatement;
import xyz.jeremydwood.school.compiler.token.Token;

import java.io.Closeable;
import java.io.IOException;
import java.io.Reader;

import static xyz.jeremydwood.school.compiler.token.EnumeratedToken.ASSIGNMENT;

/**
 * A parser for our fictitious language.
 * @author Jeremy Wood
 */
public class TinyParser implements Grammar, Closeable {

    @NotNull
    private final LookaheadScanner scanner;

    TinyParser(@NotNull Reader inputReader) throws IOException {
        this.scanner = new TinyScanner(inputReader);
    }

    /**
     * Parses the script this parse was created for.
     *
     * @return The result of the parse.
     * @throws IOException If anything goes wrong while reading in the script.
     */
    @NotNull
    public GrammarResult parse() throws IOException {
        scanner.nextToken();
        return new Program(this).parse(scanner);
    }

    @Override
    @NotNull
    public ProductionRule getProductionRule(@NotNull Token token, @NotNull LookaheadScanner scanner) {
        switch (token.getBaseToken()) {
            case BEGIN: return new Program(this);
            case IDENTIFIER:
                if (scanner.getLookahead().matches(ASSIGNMENT)) {
                    return new AssignmentStatement(this);
                } else {
                    return new Factor(this);
                }
            case NUMBER: return new Factor(this);
            case OPEN_PARENTHESIS: return new Factor(this);
            case READ: return new ReadStatement(this);
            case WRITE: return new WriteStatement(this);
            case IF: return new IfStatement(this);
            case WHILE: return new WhileStatement(this);
            case PLUS: return new AddOperator(this);
            case MINUS: return new AddOperator(this);
            case TIMES: return new MultiplyOperator(this);
            case DIVIDE: return new MultiplyOperator(this);
            default: return new Epsilon(this);
        }
    }

    @Override
    public void close() throws IOException {
        scanner.close();
    }
}
