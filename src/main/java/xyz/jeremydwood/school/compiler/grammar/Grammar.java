package xyz.jeremydwood.school.compiler.grammar;

import org.jetbrains.annotations.NotNull;
import xyz.jeremydwood.school.compiler.LookaheadScanner;
import xyz.jeremydwood.school.compiler.token.Token;

/**
 * A language grammar.
 * @author Jeremy Wood
 */
public interface Grammar {

    /**
     * Returns an instance of the production rule associated with the given token. If there is not an associated
     * production rule then {@link Epsilon} will be returned.
     *
     * @param token The token to find the production rule for.
     * @param scanner The token scanner which may be needed for lookahead.
     * @return An instance of the production rule associated with the given token.
     */
    @NotNull
    ProductionRule getProductionRule(@NotNull Token token, @NotNull LookaheadScanner scanner);
}
