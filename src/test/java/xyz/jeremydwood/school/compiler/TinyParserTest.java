package xyz.jeremydwood.school.compiler;

import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import xyz.jeremydwood.school.compiler.grammar.GrammarError;
import xyz.jeremydwood.school.compiler.grammar.GrammarResult;
import xyz.jeremydwood.school.compiler.grammar.ProductionResult;
import xyz.jeremydwood.school.compiler.token.ScannedToken;
import xyz.jeremydwood.school.compiler.token.Token;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author Jeremy Wood
 */
@RunWith(Parameterized.class)
public class TinyParserTest {

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {"testscript.g", true},
                {"scripts/valid_1", true},
                {"scripts/valid_2", true},
                {"scripts/valid_3", true},
                {"scripts/invalid_1", false},
                {"scripts/invalid_2", false},
                {"scripts/invalid_3", false},
                {"scripts/invalid_4", false},
        });
    }

    @NotNull
    private String inputFileName;
    private boolean expectedValid;

    public TinyParserTest(@NotNull String inputFileName, boolean expectedValid) {
        this.inputFileName = inputFileName;
        this.expectedValid = expectedValid;
    }

    TinyParser getTestParser(@NotNull String inputFileName) throws IOException {
        return new TinyParser(new FileReader(inputFileName));
    }

    void testScript(@NotNull String inputFileName, boolean expectedValid) throws Exception {
        GrammarResult result = getTestParser(inputFileName).parse();
        List<GrammarError> errors = result.getErrorsRecursively();
        if (errors.isEmpty()) {
            System.out.println("Parsed successfully!");
        } else {
            errors.forEach(e -> System.out.println(e.getMessage()));
        }
        if (expectedValid) {
            assertTrue(errors.isEmpty());
        } else {
            assertFalse(errors.isEmpty());
        }
    }

    @Test
    public void test() throws Exception {
        testScript(inputFileName, expectedValid);
    }
}