package xyz.jeremydwood.school.compiler;

import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import xyz.jeremydwood.school.compiler.token.EnumeratedToken;
import xyz.jeremydwood.school.compiler.token.ScannedToken;
import xyz.jeremydwood.school.compiler.token.Token;

import java.io.Reader;
import java.io.StringReader;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static xyz.jeremydwood.school.compiler.token.EnumeratedToken.*;
import static org.junit.Assert.*;

/**
 * @author Jeremy Wood
 */
@RunWith(Parameterized.class)
public class TinyScannerTest {

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {"asdf(a+&!)", new Object[] {IDENTIFIER, OPEN_PARENTHESIS, IDENTIFIER, PLUS, ERROR, CLOSED_PARENTHESIS}},
                {"beginend", new Object[] {IDENTIFIER}},
                {"&!&!", new Object[] {ERROR}},
                {"begin end", new Object[] {BEGIN, END}},
                {":=", new Object[] {ASSIGNMENT}},
                {"a:=2", new Object[] {IDENTIFIER, ASSIGNMENT, NUMBER}},
                {"a := 2", new Object[] {IDENTIFIER, ASSIGNMENT, NUMBER}},
                {"a+!!+b", new Object[] {IDENTIFIER, PLUS, ERROR, PLUS, IDENTIFIER}},
                {"a b 123 b123 123b 123b123", new Object[] {IDENTIFIER, IDENTIFIER, NUMBER, IDENTIFIER, ERROR, ERROR}},
                {"(a;b+123/b123-123b*123b123:=^^)", new Object[] {OPEN_PARENTHESIS, IDENTIFIER, STATEMENT_TERMINATOR,
                        IDENTIFIER, PLUS, NUMBER, DIVIDE, IDENTIFIER, MINUS, ERROR, TIMES, ERROR, ASSIGNMENT, ERROR,
                        CLOSED_PARENTHESIS}},
                {"1234+531", new Object[] {NUMBER, PLUS, NUMBER}},
                {"1234 + 531", new Object[] {NUMBER, PLUS, NUMBER}},
                {"1234 +/ 531", new Object[] {NUMBER, PLUS, DIVIDE, NUMBER}},
                {"", new Object[] {}},
                {Arrays.stream(EnumeratedToken.values())
                        .filter(t -> t.isListable() && t.isNonTerminal())
                        .map(EnumeratedToken::getValue)
                        .collect(Collectors.joining(" ")),
                        Arrays.stream(EnumeratedToken.values())
                                .filter(t -> t.isListable() && t.isNonTerminal())
                                .toArray()},
        });
    }

    private String input;
    private Token[] expected;

    public TinyScannerTest(String input, Object[] expected) {
        this.input = input;
        this.expected = new Token[expected.length];
        for (int i = 0; i < expected.length; i++) {
            this.expected[i] = (Token) expected[i];
        }
    }

    Reader getTestReader(@NotNull String input) {
        return new StringReader(input);
    }

    void assertSameTokens(List<Token> actual, Token... expected) {
        assertEquals(Arrays.asList(expected), actual.stream().map(token ->
                token instanceof ScannedToken ? ((ScannedToken) token).getBaseToken() : token
        ).collect(Collectors.toList()));
    }

    void testInput(@NotNull String input, Token... expected) throws Exception {
        List<Token> allTokens = TinyScanner.getAllTokens(getTestReader(input));
        System.out.println(input + " => " + allTokens);
        assertSameTokens(TinyScanner.getAllTokens(getTestReader(input)), expected);
    }

    @Test
    public void test() throws Exception {
        testInput(input, expected);
    }
}